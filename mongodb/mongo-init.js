const DB_USERNAME = process.env['MONGO_DB_USERNAME'];
const DB_PASSWORD = process.env['MONGO_DB_PASSWORD'];
const DB_NAME = process.env['MONGO_DB_NAME'];

db = db.getSiblingDB(DB_NAME);

db.createUser({
    user: DB_USERNAME,
    pwd: DB_PASSWORD,
    roles: [
        {
            role: 'readWrite',
            db: DB_NAME,
        },
    ],
});

db.auth(DB_USERNAME,DB_PASSWORD);

